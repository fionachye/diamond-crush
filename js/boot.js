var bootState = 
{
	init: function()
	{
	    this.scale.pageAlignHorizontally = true;
	    this.scale.pageAlignVertically = true;
	    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	},

	preload: function()
	{
		this.load.image('loading','assets/loading_bar.png');
		this.load.image('play','assets/play.png');
		this.load.image('play_pressed','assets/play_pressed.png');
	},

	create: function()
	{
	  	this.game.stage.backgroundColor = '#000';
	  	this.state.start('preloadState');
	},
};
