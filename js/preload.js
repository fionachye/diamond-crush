var preloadState = 
{
	init: function()
	{
		// Create this loading bar without adding it to the game world
		this.loading = this.make.sprite(game.world.centerX,game.world.centerY - 200 ,'loading');
		this.loading.anchor.setTo(0.5);
	},

	preload: function()
	{		
		this.add.existing(this.loading);
		this.load.setPreloadSprite(this.loading);
		this.loadingText = this.add.text(game.world.centerX ,game.world.centerY -250,"Loading....",style = {font: '35px Cursive', fill: '#FFF'});
		this.loadingText.anchor.setTo(0.5);
		this.load.image('background','assets/background.png');
	
		// Now load the game assets
		// Images
		this.load.image('red','assets/red.png');
		this.load.image('yellow','assets/yellow.png');
		this.load.image('blue','assets/blue.png');
		this.load.image('green','assets/green.png');
		this.load.image('square_play','assets/square_play.png');
		this.load.image('square_play_pressed','assets/square_play_pressed.png');
		this.load.image('square_pause','assets/square_pause.png');
		this.load.image('square_pause_pressed','assets/square_pause_pressed.png');
		this.load.image('continue','assets/continue.png');
		this.load.image('continue_pressed','assets/continue_pressed.png');

		// Audio files
		this.load.audio('button_click','assets/button_click.ogg');
		this.load.audio('button_hover','assets/button_hover.ogg');
		this.load.audio('gem_sound','assets/gem_sound.ogg');
		this.load.audio('error_sound','assets/error.ogg');
	},

	create: function()
	{
		this.loadingText.setText("Ready!");
		this.play = this.make.sprite(game.world.centerX ,game.world.centerY,'play');
		this.play_pressed = this.make.sprite(game.world.centerX ,game.world.centerY,'play_pressed');
		this.play_pressed.visible = false;
		this.add.existing(this.play).anchor.setTo(0.5);
		this.add.existing(this.play_pressed).anchor.setTo(0.5);
		this.button_hover_sound = this.add.audio('button_hover');
		this.button_click_sound = this.add.audio('button_click');

		this.play.inputEnabled = true; 		
		this.play.events.onInputOver.add(function(sprite){

			sprite.alpha=0.4;
			this.button_hover_sound.play();

		},this);

		this.play.events.onInputOut.add(function(sprite){

			sprite.alpha = 1.0;

		});

		this.play.events.onInputDown.add( function(sprite){
			sprite.visible = false;
			this.play_pressed.visible  = true;
            this.button_click_sound.play();

		},this);

		this.play.events.onInputUp.add(function(sprite){
			sprite.events.onInputOver.removeAll();
			sprite.visible=true;
			this.play_pressed.visible = false;
			this.time.events.loop(500,function(){
				this.state.start('gameState');

			},this);

		},this);
	},
};

