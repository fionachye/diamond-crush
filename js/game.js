var gameProperties =
{
	gameWidth: 432,
	gameHeight: 800,
	tileWidth: 72,
	tileHeight: 71,
};

var gameState = function()
{
	this.tileGrid = [
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
		[null, null, null, null, null, null, null, null],
	];
	this.tileType = ['red', 'yellow', 'blue','green'];
	this.offsetX =  gameProperties.tileWidth/2;
	this.offsetY =  gameProperties.tileHeight/2;
	this.matches = [];
	this.score = 0;
	this.highScore = 0;

	// Sounds
	this.gem_sound;
	this.error_sound;
	this.button_click_sound;
	this.button_hover_sound;

	// Buttons
	this.square_play;
	this.square_play_pressed;
	this.pause;
	this.pause_pressed;

	// Texts
	this.scoreText;
	this.timerText;

	// Timer
	this.timer = 10;
	this.timerObject;
};

gameState.prototype =
{
	init: function(route)
	{
		if (route)
		{
			if (route.from == "home")
			{
				this.resetGameVars();
			}
		}
	},

	create: function()
	{
		this.addGameAssets();
		this.initTiles();
		this.addGameScoreText();
		this.addCountdownTimer();
	},

	update: function()
	{
		this.timerText.setText('Time left: ' + this.timer + " sec");
		// TODO: Add score and timer
		if(this.score < 0)
		{
			// remove all listeners to click and hover events
			this.removeListeners();
			this.game.time.events.add(1000, this.gameOver, this);
		}

		if (this.timer < 1)
		{
			this.removeListeners();
			this.timer = 0;
			this.time.events.add(1000,this.scoreBoard,this);
		}

	},

	initTiles: function ()
	{
		var rnd;

		for (var i = 1; i < this.tileGrid.length-1; i++)
		{
			for (var j = 1; j < this.tileGrid[i].length-1; j++)
			{
				// Generate number from 0-3
				rnd = Math.floor(Math.random()*4);
				this.tileGrid[i][j] = this.add.sprite((j-1)*gameProperties.tileWidth,i*gameProperties.tileHeight+this.offsetY,this.tileType[rnd]);
				this.tileGrid[i][j].i = i;
				this.tileGrid[i][j].j = j;
				this.tileGrid[i][j].marked = false;
				this.tileGrid[i][j].inputEnabled = true;
				this.tileGrid[i][j].events.onInputDown.add(this.onClickDetected,this);
				this.tileGrid[i][j].events.onInputOver.add(this.onMouseOver,this);
				this.tileGrid[i][j].events.onInputOut.add(this.onMouseOut,this);
			}
		}

	},

	onClickDetected: function(sprite,pointer)
	{

		this.findMatch(sprite.i,sprite.j);
		this.resetTiles();

		// Do nothing there is only one or two matches found
		if (this.matches.length > 2)
		{
			this.gem_sound.play();
			this.removeTiles();
		}

		else
		{
			this.error_sound.play();
		}
		this.updateScore();
		this.matches = [];
		this.moveTilesDown();

		this.game.time.events.add(500,this.fillTiles,this);
	},

	onMouseOver: function(sprite,pointer)
	{
		sprite.alpha = 0.5;
	},

	onMouseOut: function(sprite,pointer)
	{
		sprite.alpha = 1.0;
	},

	resetTiles: function()
	{
		for (var i = 1; i < this.tileGrid.length -1 ; i++)
		{
			for (var j = 1; j < this.tileGrid[i].length -1 ; j++)
			{
				if (this.tileGrid[i][j])
				{
					this.tileGrid[i][j].marked=false;
				}
			}
		}
	},

	removeTiles: function(all)
	{
		if (all == undefined) { all = false; }

		// Only remove tiles that are same colour in groups
		if (all==false)
		{
			for (var i = 0; i < this.matches.length; i ++)
			{
				var i_ = this.matches[i].i;
				var j_ = this.matches[i].j;
				if (this.tileGrid[i_][j_])
				{
					this.tileGrid[i_][j_].destroy();
					this.tileGrid[i_][j_] = null;
				}
			}
		}

		// This is for in case user keep hitting the reshuffle button.
		// Can't restart this state because the timer will freeze while as long as user spams the reshuffle button.
		// Need to manual remove and recreate the gems instead.
		else
		{
			for (var i = 1; i < this.tileGrid.length-1; i++)
			{
				if (this.tileGrid[i])
				{
					for (var j =1; j < this.tileGrid[i].length-1; j++)
					{
						if (this.tileGrid[i][j])
						{
							this.tileGrid[i][j].destroy();
							this.tileGrid[i][j] = null;
						}
					}
				}
			}
		}
	},

	moveTilesDown: function()
	{


		for (var j = 1; j < this.tileGrid[0].length -1 ; j++)
		{
			for (var i = this.tileGrid.length-2; i > 0 ; i--)
			{
				if(this.tileGrid[i][j] == null && this.tileGrid[i-1][j] != null)
				{
					var tempTile = this.tileGrid[i-1][j];
					this.tileGrid[i][j] = tempTile;
					this.tileGrid[i][j].i = this.tileGrid[i][j].i+1;
					this.tileGrid[i-1][j] = null;
					this.add.tween(this.tileGrid[i][j]).to({y:gameProperties.tileHeight*this.tileGrid[i][j].i+this.offsetY}, 200, Phaser.Easing.Linear.In, true);

					// Note: this is really important because this for loop will decrement i again after this line!!!
					i = this.tileGrid.length-1;

				}

			}
		}

	},

	fillTiles: function()
	{
		var rnd;

		for (var i = 1; i < this.tileGrid.length-1; i++)
		{
			for (var j = 1; j < this.tileGrid[i].length-1; j++)
			{
				// Generate number from 0-2
				if (this.tileGrid[i][j] == null)
				{
					rnd = Math.floor(Math.random()*4);
					this.tileGrid[i][j] = this.add.sprite((j-1)*gameProperties.tileWidth,0,this.tileType[rnd]);
					this.add.tween(this.tileGrid[i][j]).to({y:(gameProperties.tileHeight*i+this.offsetY)}, 200, Phaser.Easing.Linear.In, true);
					this.tileGrid[i][j].i = i;
					this.tileGrid[i][j].j = j;
					this.tileGrid[i][j].marked = false;
					this.tileGrid[i][j].inputEnabled = true;
					this.tileGrid[i][j].events.onInputDown.add(this.onClickDetected,this);
					this.tileGrid[i][j].events.onInputOver.add(this.onMouseOver,this);
					this.tileGrid[i][j].events.onInputOut.add(this.onMouseOut,this);
				}
			}
		}
	},

	findMatch: function(i,j)
	{
		this.tileGrid[i][j].marked = true;

		if (this.tileGrid[i+1][j] && this.tileGrid[i+1][j].key == this.tileGrid[i][j].key && this.tileGrid[i+1][j].marked == false )
		{
			this.findMatch(i+1,j);
		}

		if (this.tileGrid[i-1][j] && this.tileGrid[i-1][j].key == this.tileGrid[i][j].key && this.tileGrid[i-1][j].marked == false )
		{
			this.findMatch(i-1,j);
		}

		if (this.tileGrid[i][j+1] && this.tileGrid[i][j+1].key == this.tileGrid[i][j].key && this.tileGrid[i][j+1].marked == false )
		{
			this.findMatch(i,j+1);
		}

		if (this.tileGrid[i][j-1] && this.tileGrid[i][j-1].key == this.tileGrid[i][j].key && this.tileGrid[i][j-1].marked == false )
		{
			this.findMatch(i,j-1);
		}

		this.matches.push(this.tileGrid[i][j]);
		return;
	},

	addGameAssets: function()
	{
		var txt;
		var style = {font: '15px arial', fill: '#fff'};

		// Background image
		this.add.sprite(0, 0, 'background');

		// Sound files
		// Need to add audio here because cache doesnt provide the "play()" method, instead we get only sound data
		this.gem_sound = this.add.audio('gem_sound');
		this.error_sound = this.add.audio('error_sound');
		this.button_click_sound = this.add.audio('button_click');
		this.button_hover_sound = this.add.audio('button_hover');

		// Play button
		this.square_play = this.add.sprite(game.world.width-110 , game.world.height -80, 'square_play');
		this.square_play.anchor.setTo(0.5);
		this.square_play_pressed = this.add.sprite(game.world.width -110, game.world.height -80, 'square_play_pressed');
		this.square_play_pressed.anchor.setTo(0.5);
		txt = this.game.add.text(game.world.width-110,game.world.height-40,"reshuffle",style);
		txt.anchor.setTo(0.5);
		this.square_play_pressed.visible = false;
		this.square_play.inputEnabled = true;

		// Pause button
		this.pause = this.add.sprite(110, game.world.height-80, 'square_pause');
		this.pause.anchor.setTo(0.5);
		this.pause_pressed = this.add.sprite(110, game.world.height-80, 'square_pause_pressed');
		this.pause_pressed.anchor.setTo(0.5);
		txt = this.add.text(110,game.world.height-40,"pause",style);
		txt.anchor.setTo(0.5);
		this.pause_pressed.visible = false;
		this.pause.inputEnabled = true;

		this.square_play.events.onInputOver.add(function(sprite){
			sprite.alpha=0.7;
		},this);

		this.square_play.events.onInputOut.add(function(sprite){
			sprite.alpha = 1.0;
		},this);

		this.square_play.events.onInputDown.add(function(sprite){
			sprite.visible = false;
			this.square_play_pressed.visible  = true;
			this.button_click_sound.play();
			this.reshuffleTiles();
		},this);

		this.square_play.events.onInputUp.add(function(sprite){
			this.square_play_pressed.visible = false;
			sprite.visible = true;
			sprite.alpha=1.0;
		},this);

		this.pause.events.onInputOver.add(function(sprite){
			sprite.alpha = 0.7;
		},this);

		this.pause.events.onInputOut.add(function(sprite){
			sprite.alpha = 1.0;
		},this);

		this.pause.events.onInputDown.add(function(sprite){
			sprite.visible = false;
			this.pause_pressed.visible = true;
			this.pause.visible = false;
			this.button_click_sound.play();

			// need use stop instead of resume.
			this.timerObject.timer.stop(false);
			this.pauseListeners();
			this.continueButton = this.add.sprite(game.world.centerX, game.world.centerY, 'continue');
			this.continueButton.anchor.setTo(0.5);
			this.continueButton.inputEnabled = true;
			this.continueButton.events.onInputOver.add(function(sprite){
				sprite.alpha = 0.7;
				},this);
			this.continueButton.events.onInputOut.add(function(sprite){
				sprite.alpha = 1.0;
			},this);
			this.continueButton.events.onInputDown.add(function(sprite){

				// Unroll the pause button state
				this.pause.visible = true;
				this.pause_pressed.visible = false;

				// Reenable input on all buttons and gems
				this.resumeListeners();
				this.timerObject.timer.start();
				// Destroy sprite after 1 second user clicked
				this.time.events.add(500,function(){sprite.destroy();},this);

				},this);

		},this);
	},

	addGameScoreText: function()
	{
		var style = {font: '25px Arial', fill: '#fff', align: 'left'};
		this.scoreText = this.add.text(game.world.centerX-150,30,"Score: "+this.score,style);
		this.scoreText.anchor.setTo(0.5);

		style = {font: '25px Arial', fill: '#fff', align: 'right'}
		this.highScoreText = this.add.text(game.world.centerX+120,30,"High Score: "+this.highScore,style);
		this.highScoreText.anchor.setTo(0.5);

	},

	addCountdownTimer: function()
	{
		// Add timer text
		var styleObject = {font: '25px Arial', fill: '#fff'}
		this.timerText = this.add.text(game.world.centerX,75,"Time left: " + this.timer + " sec" ,styleObject);
		this.timerText.anchor.setTo(0.5);
		// count down every seconds
		this.timerObject = this.time.events.loop(1000,this.updateTimer,this);
	},

	updateTimer: function()
	{
		this.timer -= 1;
	},

	updateScore: function()
	{
		var multiplier = this.matches.length;
		var scorePerGem = 10/3.0;
		if (this.matches.length > 2)
		{
			this.score += parseInt(multiplier*scorePerGem);
		}

		else { this.score -= 10; }
		this.scoreText.setText('Score: '+this.score);

	},

	removeListeners: function()
	{
		for (var i = 1; i < this.tileGrid.length-1; i ++)
		{
			if (this.tileGrid[i] !== null)
			{
				for (var j = 1; j < this.tileGrid[i].length-1; j ++)
				{
					if (this.tileGrid[i][j] !== null)
					{
						this.tileGrid[i][j].events.onInputDown.removeAll();
						this.tileGrid[i][j].events.onInputUp.removeAll();
						this.tileGrid[i][j].events.onInputOver.removeAll();
						this.tileGrid[i][j].events.onInputOut.removeAll();
					}


				}
			}
		}
	},

	pauseListeners: function()
	{
		for (var i = 1; i < this.tileGrid.length-1; i ++)
		{
			if (this.tileGrid[i] !== null)
			{
				for (var j = 1; j < this.tileGrid[i].length-1; j ++)
				{
					if (this.tileGrid[i][j] !== null)
					{
						this.tileGrid[i][j].input.enabled = false;
					}


				}
			}
		}

		this.square_play.input.enabled = false;
		this.pause.input.enabled = false;
	},

	resumeListeners: function()
	{
		for (var i = 1; i < this.tileGrid.length-1; i ++)
		{
			if (this.tileGrid[i] !== null)
			{
				for (var j = 1; j < this.tileGrid[i].length-1; j ++)
				{
					if (this.tileGrid[i][j] !== null)
					{
						this.tileGrid[i][j].input.enabled = true;
					}


				}
			}
		}

		this.square_play.input.enabled = true;
		this.pause.input.enabled = true;

	},

	reshuffleTiles: function()
	{
		this.removeListeners();
		this.removeTiles(true);
		this.initTiles();

	},

	resetGameVars: function()
	{
		if (this.score > this.highScore)
		{
			this.highScore = this.score;
		}

		this.matches = [];
		this.score = 0;
		this.timer = 10;

	},

	gameOver: function()
	{
		this.state.start('homeState',true,false,message = {status: "game over"});
	},

	scoreBoard: function()
	{
		if (this.score >= this.highScore)
		{
			this.highScore = this.score;
		}

		this.state.start('homeState',true,false, message = {status: "score",score: this.score, highScore: this.highScore});
	},
};
