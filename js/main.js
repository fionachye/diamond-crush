//initiate the Phaser framework
var game = new Phaser.Game(gameProperties.gameWidth,gameProperties.gameHeight,Phaser.AUTO);

game.state.add('gameState', gameState);
game.state.add('preloadState', preloadState);
game.state.add('bootState', bootState);
game.state.add('homeState',homeState);
game.state.start('bootState');