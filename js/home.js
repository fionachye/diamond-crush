var homeState = 
{
	init: function(message)
	{
		this.message = message;
	},

	preload: function()
	{
		this.background = this.add.sprite(0, 0, 'background');
	},

	create: function()
	{
		var style = {font: '20px Arial', fill: '#FFF', align: 'center'}; 
		this.background.inputEnabled = true; 

		if (this.message.status == "game over")
		{
			this.text = this.add.text(game.world.centerX,game.world.centerY,"Game over! \nClick screen to play again.",style);
			this.text.anchor.setTo(0.5);

		}

		else if (this.message.status == "score")
		{
			this.text = this.add.text(game.world.centerX,game.world.centerY,
				"Your final score is " + this.message.score + ". \nHigh Score is " + this.message.highScore + "\nClick screen to play again.",style);
			this.text.anchor.setTo(0.5);	
		}

		this.background.events.onInputDown.add(function(){
			this.state.start('gameState',true,false,route = {from: "home"});
		},this);
	},

	update: function()
	{

	},
};
